Employees: Employees.cpp 
  g++ -o Employees Employees.cpp \ 
  -I/usr/include/oracle/11.1/client \ 
  -L$(ORACLE_HOME)/lib -lclntsh -locci 

debug: Employees.cpp 
  g++ -ggdb3 -o Employees Employees.cpp \ 
  -I/usr/include/oracle/11.1/client \ 
  -L$(ORACLE_HOME)/lib -lclntsh -locci 

clean: 
  rm -f Employees