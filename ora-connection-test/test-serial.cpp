#include "tests.h"
#include "A.h"

string SerialTest(int Count,const ConnectionOptions& Options)
{
	string errorLog;
	for (size_t i=0; i<Count; ++i)
	{
		try
		{
			A a;
			a.Connect(Options.userName, Options.password, Options.databaseName);
			a.Disconnect();
		}
		catch (SQLException& ex)
		{
			errorLog +=  ex.getMessage() + "\n";
			return errorLog;
		}
	}
	return errorLog;
};

