#include "A.h"
#include "env.h"

A::A():_connection(0) 
{ 
} 

A::~A() 
{ 
	if ( _connection )
		Disconnect();
} 

void 
	A::Connect(const string UserName, const string Password, const string DatabaseName)
{
	if ( _connection )
		Disconnect();
	_connection = env()->createConnection(UserName, Password, DatabaseName); 
};

void 
	A::Disconnect()
{
	if ( _connection )
	{
		env()->terminateConnection ( _connection );
		_connection = 0;
	} 
};
