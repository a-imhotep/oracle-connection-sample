#include "A.h"
#include "env.h"
#include "tests.h"
#include <iostream>

using namespace std;

// generate help string
string Usage()
{
	return "ora-connection-test UserName Password DatabaseName";
}

int main(int argc, char** cmd)
{
	// setting defaults
	ConnectionOptions connectionOptions;
	// parsing command line
	if (argc == 1)
	{
		cout << "There was no command line parameters. Using defaults.\n";
		cout << "To use differnt parameters, please, run " << Usage() << endl;
	} 
	else if (argc != 4)
	{
		cout << Usage() << endl;
		return 1;
	}
	else
	{
		connectionOptions.userName = cmd[1];
		connectionOptions.password = cmd[2];
		connectionOptions.databaseName = cmd[3];
	}

	try
	{
		// prepare Environment
		CreateEnvironment();
		// test singe thread connections
		string serialTestLog = SerialTest( 100, connectionOptions);
		if ( serialTestLog.empty() )
			cout << "Serial test - passed\n";
		else
			cout << "Serial test failed with message:'" << serialTestLog << "'\n";

		// test paralel connections
		string paralelTestLog = ParalelTest(10, 10, connectionOptions);
		if ( paralelTestLog.empty() )
			cout << "Paralel test - passed\n";
		else
			cout << "Paralel test failed with message:'" << paralelTestLog << "'\n";
	}
	catch (SQLException& ex)
	{
		cerr << "Runtime ERROR with message '" <<  ex.getMessage() << "'\n";
		return -1;
	}
	return 1;
}
