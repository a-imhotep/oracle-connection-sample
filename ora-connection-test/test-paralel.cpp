#include "tests.h"
#include "A.h"

#include <pthread.h>
#include <vector>
#include <errno.h>
#include <string.h>

namespace
{

	struct ThreadInfo
	{
		ThreadInfo()
		{
			pthread_mutex_init(&mutex, 0);
		}
		~ThreadInfo()
		{
			pthread_mutex_destroy(&mutex);
		}
		string errorLog;
		pthread_mutex_t mutex;
		int connectionCount;
		ConnectionOptions options;
	};

	void * ThreadFunction(void *p)
	{
		ThreadInfo* threadInfo = reinterpret_cast<ThreadInfo*>(p);
		for (size_t i=0; i<threadInfo->connectionCount; ++i)
		{
			try
			{
				A a;
				a.Connect(threadInfo->options.userName, threadInfo->options.password, threadInfo->options.databaseName);
				a.Disconnect();
			}
			catch (SQLException& ex)
			{
				pthread_mutex_lock(&threadInfo->mutex);
				threadInfo->errorLog +=  ex.getMessage() + "\n";
				pthread_mutex_unlock(&threadInfo->mutex);
				return 0;
			}
		}
	}
};

string ParalelTest(int ThreadCount, int ConnectionCount, const ConnectionOptions& Options)
{
	vector<pthread_t> threadHandlers;
	ThreadInfo info;
	info.options = Options;
	info.connectionCount = ConnectionCount;
	// create threads
	for (size_t i=0; i< ThreadCount; ++i)
	{
		pthread_t th;
		int res = pthread_create(&th, 0, ThreadFunction, &info);
		if ( res )
		{
			pthread_mutex_lock(&info.mutex);
			info.errorLog += string(strerror(res)) + "\n";
			pthread_mutex_unlock(&info.mutex);
		}
		else
			threadHandlers.push_back(th);
	}
	// wait for threads completition
	for (size_t i=0; i< ThreadCount; ++i)
	{
		int res = pthread_join(threadHandlers[i], 0);
		if ( res )
		{
			pthread_mutex_lock(&info.mutex);
			info.errorLog += string(strerror(res)) + "\n";
			pthread_mutex_unlock(&info.mutex);
		}
	}

	return info.errorLog;
};

