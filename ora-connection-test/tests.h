#ifndef TESTS_HPP
#define TESTS_HPP

#include <string>
using namespace std;

struct ConnectionOptions
{
	ConnectionOptions()
	{
		userName = "otk"; 
		password = "otk"; 
		databaseName = "OracleDB:1531/PROD"; 
	};
	string userName; 
	string password;
	string databaseName;
};

string SerialTest(int Count, const ConnectionOptions& Options);
string ParalelTest(int ThreadCount, int ConnectionCount, const ConnectionOptions& Options);

#endif//TESTS_HPP
