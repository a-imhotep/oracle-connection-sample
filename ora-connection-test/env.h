#ifndef ENV_HPP
#define ENV_HPP

#include <occi.h> 
using namespace oracle::occi; 

// global environment object
Environment* env();
// create invironment instance
void CreateEnvironment();

#endif//ENV_HPP
