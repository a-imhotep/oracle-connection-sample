#include "env.h"

namespace
{
	struct EnvInstance
	{
		EnvInstance():env(0)
		{
		}

		~EnvInstance()
		{
			if ( env )
				Environment::terminateEnvironment ( env ); 
		}

		void Create()
		{
			env = Environment::createEnvironment(Environment::THREADED_MUTEXED); 
		}

		Environment *env;
	};

	EnvInstance envInstance;
}

Environment* env()
{
	return envInstance.env;
};

void CreateEnvironment()
{
	envInstance.Create();
};


