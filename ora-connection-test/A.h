#ifndef A_HPP
#define A_HPP

#include <occi.h> 

using namespace oracle::occi; 
using namespace std; 

class A 
{ 
public: 
	A(); 
	virtual ~A(); 
	void Connect(const string UserName, const string Password, const string DatabaseName);
	void Disconnect();
private: 
	Connection  *_connection; 
};
#endif//A_HPP
